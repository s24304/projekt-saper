#include <iostream>
#include <utility>
#include <vector>
using namespace std;
 
const int Easy = 1;
const int Hard = 2;
const int MRozmiar= 30;
 
int Wiersze;
int Kolumny;
int Miny;
 
void TrybGry() // Wybór trybu gry
{
    int Tryb;
 
    cout << "Wybierz Tryb Gry" << endl;
    cout << "Wciśnij 1 by zagrać planszę 10 * 10 z 15 minami" << endl;
    cout << "Wciśnij 2 by zagrać planszę 30 * 30 z 99 minami" << endl;
 
    cin >> Tryb;
 
    if (Tryb == Easy) 
    {
        Wiersze = 10;
        Kolumny = 10;
        Miny = 15;
    }
 
    if (Tryb == Hard) 
    {
        Wiersze = 30;
        Kolumny = 30;
        Miny = 150;
    }
 
    return;
}
 
void Plansza(char PlanszaMin[][MRozmiar], char PlanszaGry[][MRozmiar]) // Generowanie planszy
{
    for (int i = 0; i < Wiersze; i++)
    for (int j = 0; j < Kolumny; j++)
    PlanszaGry[i][j] = PlanszaMin[i][j] = '.';
    return;
}
 
void RozMiny(char PlanszaMin[][MRozmiar], int miny) //Generowanie min
{
    int stawione = 0;
    while (stawione < miny) 
    {
        int Los = rand() % (Wiersze * Kolumny);
        int Wier = Los / Kolumny;
        int Kol = Los % Wiersze;
        if (PlanszaMin[Wier][Kol] == '#') continue; // Jeżeli jest już mina
        PlanszaMin[Wier][Kol] = '#';
        stawione++;
    }
    return;
}
void ZastMiny(int Wier, int Kol, char PlanszaMin[][MRozmiar]) 
{
    RozMiny(PlanszaMin, 1);
    PlanszaMin[Wier][Kol] = '.';
    return;
}
 
char indexToChar(int index) 
{
    if (index < 10)
    return index + '0';
    else
    return 'a' + (index - 10);
}
 
int charToIndex(char ch) 
{
    if (ch <= '9')
    return ch - '0';
    else
    return (ch - 'a') + 10;
}
 
void WPlansza(char PlanszaGry[][MRozmiar]) 
{
    // Górna linia
    cout << "    ";
    for (int i = 0; i < Kolumny; i++)
    cout << indexToChar(i) << ' ';
    cout << endl << endl;
 
    // Boczna linia
    for (int i = 0; i < Wiersze; i++) 
    {
        cout << indexToChar(i) << "   ";
        for (int j = 0; j < Kolumny; j++)
        cout << PlanszaGry[i][j] << " ";
        cout << "  " << indexToChar(i);
        cout << endl;
    }
 
    // Dolna linia
    cout << endl << "    ";
    for (int i = 0; i < Kolumny; i++)
        cout << indexToChar(i) << ' ';
    cout << endl;
 
    return;
}
 
bool Blad(int Wier, int Kol) 
{
    return (Wier >= 0) && (Wier < Wiersze) && (Kol >= 0) && (Kol < Kolumny);
}
 
bool JMina(int Wier, int Kol, char Tablica[][MRozmiar]) 
{
    return (Tablica[Wier][Kol] == '#');
}
 
vector < pair <int, int> > WMiny(int Wier, int Kol) 
{

    vector < pair <int, int> > SMiny;
 
    for (int dx = -1; dx <= 1; dx++)
    for (int dy = -1; dy <= 1; dy++)
    if (dx != 0 || dy != 0)
    if (Blad(Wier+dx, Kol+dy))
    SMiny.push_back(make_pair(Wier+dx, Kol+dy)); // Dodaj na końcu i połącz
 
    return SMiny;
}
 
int LSMiny(int Wier, int Kol, char PlanszaMin[][MRozmiar]) // Liczy miny na sąsiednich polach
{
    vector < pair <int, int> > SMiny = WMiny(Wier, Kol);
 
    int Licz = 0;
    for (int i = 0; i < SMiny.size(); i++)
    if (JMina(SMiny[i].first, SMiny[i].second, PlanszaMin))
    Licz++;
 
    return Licz;
}
 
void OdPlansza(char PlanszaGry[][MRozmiar], char PlanszaMin[][MRozmiar], int Wier, int Kol, int *Ruch) // Jeżeli pole = 0, odkrywa wszystkie sąsiadujące puste pola
{
    (*Ruch)++;
    int Licz = LSMiny(Wier, Kol, PlanszaMin);
    PlanszaGry[Wier][Kol] = Licz + '0';
 
    if (Licz == 0) 
    {
        vector < pair <int, int> > SMiny = WMiny(Wier, Kol);
 
        for (int i = 0; i < SMiny.size(); i++)
        if (PlanszaGry[SMiny[i].first][SMiny[i].second] == '.')
        OdPlansza(PlanszaGry, PlanszaMin, SMiny[i].first, SMiny[i].second, Ruch);
    }
 
    return;
}
 
void ZaznMiny(char PlanszaGry[][MRozmiar], char PlanszaMin[][MRozmiar], bool w) 
{
    for (int i = 0; i < Wiersze; i++) 
    {
        for (int j = 0; j < Kolumny; j++) 
        {
            if (PlanszaGry[i][j] == '.' && PlanszaMin[i][j] == '#') 
            {
                if (w) 
                {
                    PlanszaGry[i][j] = 'F';
                }
                else 
                {
                    PlanszaGry[i][j] = '#';
                }
            }
        }
    }
}
 
void Saper() 
{
    char PlanszaMin[MRozmiar][MRozmiar], PlanszaGry[MRozmiar][MRozmiar];
    int MRuch = Wiersze * Kolumny - Miny;
    int Flagi = Miny;
    Plansza(PlanszaMin, PlanszaGry);
    RozMiny(PlanszaMin, Miny);
 
    int Ruch = 0;
    bool Koniec = false;
 
    while (!Koniec) 
    {
        WPlansza(PlanszaGry);
        cout << Flagi << " Pozostałe flagi" << endl << endl;

        char x, y, z;
        cout << "Wpisz swój ruch, (wiersz, kolumna, bezpieczne(p)/flaga(f)) -> ";
        cin >> x >> y >> z;
        cout << endl;
 
        int Wier = charToIndex(x);
        int Kol = charToIndex(y);
 
        if (Ruch == 0)
        if (JMina(Wier, Kol, PlanszaMin))
        ZastMiny(Wier, Kol, PlanszaMin);
 
        if (z == 'p') 
        {
            if (PlanszaGry[Wier][Kol] == '.' && PlanszaMin[Wier][Kol] == '.') 
            {
                OdPlansza(PlanszaGry, PlanszaMin, Wier, Kol, &Ruch);
                if (Ruch == MRuch) 
                {
                    ZaznMiny(PlanszaGry, PlanszaMin, true);
                    WPlansza(PlanszaGry);
                    cout << endl << "Wygrałeś!!! :)" << endl;
                    Koniec = true;
                }
            }
            else if (PlanszaGry[Wier][Kol] == '.' && PlanszaMin[Wier][Kol] == '#') 
            {
                // Koniec gry
                PlanszaGry[Wier][Kol] = '#';
                ZaznMiny(PlanszaGry, PlanszaMin, false);
                WPlansza(PlanszaGry);
                cout << endl << "Przegrałeś! :(" << endl;
                Koniec = true;
            }
            else 
            {
                // Zły ruch
                cout << "Zły ruch. ";
                if (PlanszaGry[Wier][Kol] == 'F')
                    cout << "Pole jest flagą. Użyj f żeby odznaczyć. ";
                else
                    cout << "Pole jest juz numerem. ";
                cout << endl;
            }
        }
 
        if (z == 'f') 
        {
            if (PlanszaGry[Wier][Kol] == '.') 
        {
                if (Flagi != 0) 
                {
                    PlanszaGry[Wier][Kol] = 'F';
                    Flagi--;
                }
                else 
                {
                    cout << "Zły ruch. Za dużo flag!" << endl;
                }
            }
            else if (PlanszaGry[Wier][Kol] == 'F') 
            {
                PlanszaGry[Wier][Kol] = '.';
                Flagi++;
            }
            else 
            {
                cout << "Zły ruch. Pole jest numerem. " << endl;
            }
        }
    }
 
    return;
}
 
int main() 
{
    srand(time(NULL));
    TrybGry();
    Saper();
    return 0;
}
